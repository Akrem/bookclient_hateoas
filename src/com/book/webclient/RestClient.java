/**
 * 
 */
package com.book.webclient;

import java.util.ArrayList;
import java.util.List;

import javax.xml.ws.Response;

import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxrs.client.WebClient;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;

import com.book.webclient.OrderRequest;

/**
 * @author developer
 *
 */
public class RestClient {

	public String BookServiceAddress = "http://localhost:8080";
	public String BookServiceStartingPoint = "/bookservice/isbn/";
	/**
	 * 
	 */
	public RestClient() {
		// TODO Auto-generated constructor stub
	}
	
	public String getBookByISBN(int isbn){
		List<Object> providers = new ArrayList<Object>();
        JacksonJsonProvider provider = new JacksonJsonProvider();
        provider.addUntouchable(Response.class);
        providers.add(provider);
               
        WebClient getClient = WebClient.create(BookServiceAddress, providers);
        
        //Configuring the CXF logging interceptor for the outgoing message
        WebClient.getConfig(getClient).getOutInterceptors().add(new LoggingOutInterceptor());
        //Configuring the CXF logging interceptor for the incoming response
        WebClient.getConfig(getClient).getInInterceptors().add(new LoggingInInterceptor());
        
        // change application/xml  to get in xml format
        getClient = getClient.accept("application/json").type("application/json").path(BookServiceStartingPoint+isbn);
                
        //to see as raw XML/json
        String response = getClient.get(String.class);
       
        return response;
	}
	
	public String buyBook(long isbn, double payment, String shipping, String link) {
		List<Object> providers = new ArrayList<Object>();
        JacksonJsonProvider provider = new JacksonJsonProvider();
        provider.addUntouchable(Response.class);
        providers.add(provider);
        
        OrderRequest orderRequest = new OrderRequest();
    	orderRequest.setIsbn(isbn);
    	
    	orderRequest.setShippingAddress(shipping);
    	orderRequest.setPayment(payment);
        
    	 // the host
        String host = link.substring(0, 21);
     	String servicePath = link.substring(21);
        WebClient putClient = WebClient.create(host, providers);
        
        //Configuring the CXF logging interceptor for the outgoing message
        WebClient.getConfig(putClient).getOutInterceptors().add(new LoggingOutInterceptor());
        //Configuring the CXF logging interceptor for the incoming response
        WebClient.getConfig(putClient).getInInterceptors().add(new LoggingInInterceptor());
        
        // change application/xml  to get in xml format
        putClient = putClient.accept("application/json").type("application/json").path(servicePath);
                
        String response =  putClient.put(orderRequest, String.class);
       
		return response;
	}

	public String orderStatus(String link) {
		List<Object> providers = new ArrayList<Object>();
        JacksonJsonProvider provider = new JacksonJsonProvider();
        provider.addUntouchable(Response.class);
        providers.add(provider);
        
        WebClient getClient = WebClient.create(getProvider(link), providers);
        
        //Configuring the CXF logging interceptor for the outgoing message
        WebClient.getConfig(getClient).getOutInterceptors().add(new LoggingOutInterceptor());
        //Configuring the CXF logging interceptor for the incoming response
        WebClient.getConfig(getClient).getInInterceptors().add(new LoggingInInterceptor());
        
        // change application/xml  to get in xml format
        getClient = getClient.accept("application/json").type("application/json").path(getServicePath(link));
                
        String response =  getClient.get(String.class);
       
		return response;
	}

	public void cancelOrder(String link) {
		List<Object> providers = new ArrayList<Object>();
        JacksonJsonProvider provider = new JacksonJsonProvider();
        provider.addUntouchable(Response.class);
        providers.add(provider);
        
        WebClient deleteClient = WebClient.create(getProvider(link), providers);
        
      //Configuring the CXF logging interceptor for the outgoing message
        WebClient.getConfig(deleteClient).getOutInterceptors().add(new LoggingOutInterceptor());
        //Configuring the CXF logging interceptor for the incoming response
        WebClient.getConfig(deleteClient).getInInterceptors().add(new LoggingInInterceptor());
        
        // change application/xml  to application/json get in json format
        deleteClient = deleteClient.accept("application/json").type("application/json").path(getServicePath(link));
     	
        deleteClient.delete();
    	
	}
	
	public String getProvider(String link) {
		return link.substring(0, 21);
	}
	
	public String getServicePath(String link) {
		return link.substring(21);
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
	}

}
