/**
 * 
 */
package com.book.webclient;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 * @author developer
 * 
 */
public class RequestProcessorServlet {
	private int isbn;
	private long orderId;
	//private String author;
	private String booksReturned;
	private String orderReturned;
	private String statusReturned;
	private double price;
	private String shipping;
	
	private boolean showSearch = true;
	private boolean showCheckout = false;
	private boolean showPayment = false;
	private boolean showOrder = false;
	private boolean showStatus = false;
	private boolean showCancelOrder = false;
	private boolean showError = false;
	private String errorMessage = "";
	
	private RestClient restClient = null;
	private JSONObject bookOutputObject = null;
	private JSONObject orderOutputObject = null;
	private JSONObject statusOutputObject = null;
	private JSONObject cancelOutputObject = null;
	
	
	public RequestProcessorServlet() {
		super();
		restClient = new RestClient();
	}

	public String reset() {
		restClient = new RestClient();
		showSearch = true;
		showCheckout = false;
		showPayment = false;
		showOrder = false;
		showStatus = false;
		showCancelOrder = false;
		showError = false;
				
		bookOutputObject = null;
		orderOutputObject = null;
		statusOutputObject = null;
		cancelOutputObject = null;
		
		isbn = 0;
		orderId = 0;
		//private String author;
		booksReturned = "";
		orderReturned ="";
		statusReturned = "";
		price = 0.0;
		shipping = "";
		errorMessage = "";
		return "reset";
	}

	public String getBookByISBN() throws JSONException {
		showCheckout = true;
		booksReturned = restClient.getBookByISBN(isbn);
		if (booksReturned==null) {
			showError = true;
			setErrorMessage("Book search failed. Sorry please try again.");
			return null;
		}
		bookOutputObject = new JSONObject(booksReturned);
		showSearch = false;
		return "calculated";
	}

	public String checkOutBook(){
		showCheckout = false;
		showPayment = true;
		return "checkout";
	}
	
	public String buyBook() throws JSONException{
		showPayment = false;
		String link = getCorrespondingLink("buyBook", bookOutputObject);
		orderReturned = restClient.buyBook(isbn, price, shipping, link);
		
		if (orderReturned==null) {
			setShowError(true);
			setErrorMessage("Buying the book failed. Sorry please try again.");
			return null;
		}
		
		orderOutputObject = new JSONObject(orderReturned);
		orderId = orderOutputObject.getLong("orderId");
		showOrder = true;
		return "buyBook";
	}
	
	public String orderStatus() throws JSONException{
		showOrder = false;
		String link = getCorrespondingLink("orderstatus", orderOutputObject);
		statusReturned = restClient.orderStatus(link);
		
		if (statusReturned==null) {
			showError = true;
			setErrorMessage("Unable to check the status of the book. Sorry please try again.");
			return null;
		}
		
		statusOutputObject = new JSONObject(statusReturned);
		showStatus = true;
		return "orderStatus";
	}
	
	public String cancelOrder() throws JSONException{
		showStatus = false;
		String link = getCorrespondingLink("cancelorder", statusOutputObject);
		restClient.cancelOrder(link);
		setShowCancelOrder(true);
		return "cancelOrder";
	}
	/*public ArrayList<Pair> getBookByAuthor() throws JSONException {
		initial = false;
		booksReturned = restClient.getBookByAuthor(author);
		JSONArray arr = bookOutputObject.getJSONArray("links");
		JSONObject firstObj = arr.getJSONObject(0);
		return "calculated";
	}*/

	private String getCorrespondingLink(String relValue, JSONObject json ) throws JSONException {
		JSONArray arr = json.getJSONArray("links");
		for (int index = 0; index<arr.length(); index++){
			if (arr.getJSONObject(index).getString("relValue").toLowerCase().contains(relValue.toLowerCase())) {
				return arr.getJSONObject(index).getString("uri");
			}
		}
		return null;
	}
	
	public int getIsbn() {
		return isbn;
	}

	public void setIsbn(int isbn) {
		this.isbn = isbn;
	}

	public String getBooksReturned() {
		return booksReturned;
	}

	public void setBooksReturned(String booksReturned) {
		this.booksReturned = booksReturned;
	}

	public JSONObject getBookOutputObject() {
		return bookOutputObject;
	}

	public void setBookOutputObject(JSONObject bookOutputObject) {
		this.bookOutputObject = bookOutputObject;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getStatusReturned() {
		return statusReturned;
	}

	public void setStatusReturned(String statusReturned) {
		this.statusReturned = statusReturned;
	}

	public JSONObject getStatusOutputObject() {
		return statusOutputObject;
	}

	public void setStatusOutputObject(JSONObject statusOutputObject) {
		this.statusOutputObject = statusOutputObject;
	}

	public JSONObject getOrderOutputObject() {
		return orderOutputObject;
	}

	public void setOrderOutputObject(JSONObject orderOutputObject) {
		this.orderOutputObject = orderOutputObject;
	}

	public boolean isShowOrder() {
		return showOrder;
	}

	public void setShowOrder(boolean showOrder) {
		this.showOrder = showOrder;
	}

	public String getOrderReturned() {
		return orderReturned;
	}

	public void setOrderReturned(String orderReturned) {
		this.orderReturned = orderReturned;
	}

	public boolean isShowCheckout() {
		return showCheckout;
	}

	public void setShowCheckout(boolean showCheckout) {
		this.showCheckout = showCheckout;
	}

	public boolean isShowPayment() {
		return showPayment;
	}

	public void setShowPayment(boolean showPayment) {
		this.showPayment = showPayment;
	}

	public boolean isShowStatus() {
		return showStatus;
	}

	public void setShowStatus(boolean showStatus) {
		this.showStatus = showStatus;
	}

	public RestClient getRestClient() {
		return restClient;
	}

	public void setRestClient(RestClient restClient) {
		this.restClient = restClient;
	}

	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	public JSONObject getCancelOutputObject() {
		return cancelOutputObject;
	}

	public void setCancelOutputObject(JSONObject cancelOutputObject) {
		this.cancelOutputObject = cancelOutputObject;
	}

	public boolean isShowCancelOrder() {
		return showCancelOrder;
	}

	public void setShowCancelOrder(boolean showCancelOrder) {
		this.showCancelOrder = showCancelOrder;
	}

	public String getShipping() {
		return shipping;
	}

	public void setShipping(String shipping) {
		this.shipping = shipping;
	}

	public boolean isShowSearch() {
		return showSearch;
	}

	public void setShowSearch(boolean showSearch) {
		this.showSearch = showSearch;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public boolean isShowError() {
		return showError;
	}

	public void setShowError(boolean showError) {
		this.showError = showError;
	}

}