<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@ taglib prefix="h" uri="http://java.sun.com/jsf/html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>BOOKS API CLIENT</title>
<link rel="stylesheet" href="MainStyle.css" type="text/css">
</head>
<body>
	<div class="bookheader">BOOKS OF YOUR CHOICE </div>
	<f:view>
		<h:form>
		<h:panelGroup rendered="#{requestProcessorServlet.showError==true}">
			<h:panelGroup><h:inputText value="#{requestProcessorServlet.errorMessage}"></h:inputText></h:panelGroup>
			<h:commandLink value="Start Again" action="#{requestProcessorServlet.reset}"></h:commandLink>
		</h:panelGroup>
		<h:panelGroup rendered="#{requestProcessorServlet.showSearch==true}">
			<h:panelGrid columns="3" width="40%">
			<f:facet name="header"><h:outputLabel value="Order Book"></h:outputLabel></f:facet>
			<h:panelGroup><h:outputLabel value="By ISBN: "></h:outputLabel></h:panelGroup>
			<h:panelGroup><h:inputText value="#{requestProcessorServlet.isbn}"></h:inputText></h:panelGroup>
			<h:panelGroup><h:commandLink action="#{requestProcessorServlet.getBookByISBN}" value="Search"></h:commandLink> </h:panelGroup>
			<f:facet name="footer">
				<h:commandLink value="Start Again" action="#{requestProcessorServlet.reset}"></h:commandLink>
			</f:facet>
			</h:panelGrid>
		</h:panelGroup>
		
		<h:panelGroup rendered="#{requestProcessorServlet.showCheckout==true}">
			<f:facet name="checkoutheader">
				<h:outputLabel value="ISBN: + #{requestProcessorServlet.isbn}"></h:outputLabel>
			</f:facet>
			<f:facet name="found">
				<h:outputLabel value="Book Found"></h:outputLabel>
			</f:facet>
			<h:panelGrid columns="6" width="40%">
				<h:outputLabel value="ISBN"></h:outputLabel>
				<h:outputLabel value="Title"></h:outputLabel>
				<h:outputLabel value="Author"></h:outputLabel>
				<h:outputLabel value="Status"></h:outputLabel>
				<h:outputLabel value="Price"></h:outputLabel>
				<h:outputLabel></h:outputLabel>		
						
				<h:panelGroup ><h:outputLabel value="#{requestProcessorServlet.bookOutputObject.getString(\"isbn\")}"></h:outputLabel></h:panelGroup>

				<h:panelGroup><h:outputLabel value="#{requestProcessorServlet.bookOutputObject.getString(\"title\")}"></h:outputLabel></h:panelGroup>

				<h:panelGroup><h:outputLabel value="#{requestProcessorServlet.bookOutputObject.getString(\"author\")}"></h:outputLabel></h:panelGroup>

				<h:panelGroup><h:outputLabel value="#{requestProcessorServlet.bookOutputObject.getString(\"status\")}"></h:outputLabel></h:panelGroup>
				
				<h:panelGroup><h:outputLabel value="#{requestProcessorServlet.bookOutputObject.getString(\"price\")}"></h:outputLabel></h:panelGroup>
				
				<h:commandLink action="#{requestProcessorServlet.checkOutBook}" value="Order"></h:commandLink>
				<h:commandLink value="Start Again" action="#{requestProcessorServlet.reset}"></h:commandLink>
			</h:panelGrid>
		</h:panelGroup>
		<h:panelGroup rendered="#{requestProcessorServlet.showPayment==true}">
			<h:outputLabel value="ISBN: #{requestProcessorServlet.isbn}"></h:outputLabel><br />
			<h:outputLabel value="Title: #{requestProcessorServlet.bookOutputObject.getString(\"status\")}"></h:outputLabel>
			
			<h:panelGrid columns="2">				
				<h:panelGroup><h:outputLabel value="Shipping"></h:outputLabel></h:panelGroup>
				<h:panelGroup><h:inputText value="#{requestProcessorServlet.shipping}"></h:inputText></h:panelGroup>
				<h:panelGroup><h:outputLabel value="Payment: "></h:outputLabel></h:panelGroup>
				<h:panelGroup><h:outputLabel value="#{requestProcessorServlet.bookOutputObject.getString(\"price\")}"></h:outputLabel></h:panelGroup>
				<h:commandLink action="#{requestProcessorServlet.buyBook}"
				value="Place Order"></h:commandLink> 
				<h:commandLink value="Start Again" action="#{requestProcessorServlet.reset}"></h:commandLink>
			</h:panelGrid>		
		</h:panelGroup>
		
		<h:panelGroup rendered="#{requestProcessorServlet.showOrder==true}">
			<h:outputLabel value="ISBN: #{requestProcessorServlet.isbn}"></h:outputLabel><br />
					
			<h:panelGrid columns="2">				
				<h:panelGroup><h:outputLabel value="Book Order Confirmation: "></h:outputLabel></h:panelGroup>
				<h:panelGroup><h:outputLabel value="#{requestProcessorServlet.orderOutputObject.getString(\"orderId\")}"></h:outputLabel></h:panelGroup>
				<h:commandLink action="#{requestProcessorServlet.orderStatus}"
				value="Check Status"></h:commandLink> 
			</h:panelGrid>		
		</h:panelGroup>
		
		<h:panelGroup rendered="#{requestProcessorServlet.showStatus==true}">
			<h:outputLabel value="ISBN: #{requestProcessorServlet.isbn}"></h:outputLabel><br />
			<h:outputLabel value="Order ID: #{requestProcessorServlet.orderId}"></h:outputLabel>
			
			<h:panelGrid columns="2">				
				<h:panelGroup><h:outputLabel value="Order Status: "></h:outputLabel></h:panelGroup>
				<h:panelGroup><h:outputLabel value="#{requestProcessorServlet.statusOutputObject.getString(\"orderStatus\")}"></h:outputLabel></h:panelGroup>
				<h:commandLink action="#{requestProcessorServlet.cancelOrder}"
				value="Cancel Order"></h:commandLink>
				<h:commandLink value="Start Again" action="#{requestProcessorServlet.reset}"></h:commandLink>
			</h:panelGrid>		
		</h:panelGroup>
		
		<h:panelGroup rendered="#{requestProcessorServlet.showCancelOrder==true}">
			<h:outputLabel value="ISBN: #{requestProcessorServlet.isbn}"></h:outputLabel><br />
			<h:outputLabel value="Order ID: #{requestProcessorServlet.orderId}"></h:outputLabel>
			<h1> The order has successfully canceled. </h1>
			<h:commandLink value="Start Again" action="#{requestProcessorServlet.reset}"></h:commandLink>
					
		</h:panelGroup>
	</h:form>
	</f:view>
</body>
</html>
